import Head from 'next/head'
import Navigation from './Sections/Navigation'
import Footer from './Sections/Footer'

import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';



export default ({ children, title = 'Praxis für Logopädie - Simon Nüchter' }) => (
  <React.Fragment>
    <CssBaseline />

    <Head>
      <title>{ title }</title>
      <style jsx global>{`
      `}</style>
     </Head>

    <Navigation />

    <main>
      { children }
    </main>

    <Footer />

  </React.Fragment>
)
