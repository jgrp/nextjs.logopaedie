
import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import classNames from 'classnames';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';



const styles = theme => ({
  header: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 21,
    paddingBottom: theme.spacing.unit * 21,
    position: 'relative',
    '&::after': {
      content: `''`,
      display: 'block',
      position: 'absolute',
      bottom: 0,
      left: 0 ,
      top: 0,
      width: '100%' ,
      background: theme.palette.background.default,
      zIndex: '-1',
      [theme.breakpoints.up('xs')]: {
        borderRadius: '0 0 45% 30%/0 0 50px 5px',
      },
      [theme.breakpoints.up('sm')]: {
        borderRadius: '0 0 45% 30%/0 0 70px 7px',
      },
      [theme.breakpoints.up('md')]: {
        borderRadius: '0 0 45% 30%/0 0 120px 10px',
      },
      [theme.breakpoints.up('lg')]:{
        borderRadius: '0 0 45% 30%/0 0 150px 10px',
      },
    }
  },
  title: {
    fontSize:  '20px',
    marginTop:  '0'
  },
  name: {
    fontFamily: theme.typography.headline.fontFamily,
    fontSize: theme.typography.h2.fontSize,
    margin: '6px'
  }
});

class Header extends React.Component {


  render() {
    const {classes} = this.props;

    return (
      <header className={classes.header}>
        <Typography variant="h1" className={classes.title}>Praxis für Logopädie</Typography>
        <Typography className={classes.name}>Simon Nüchter & Ina Metzger</Typography>
        <Typography variant="h2" color="textPrimary" className={classes.title}>Friedberg</Typography>
      </header>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);

