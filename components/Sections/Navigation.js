/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link'
import {AppBar, Toolbar, SwipeableDrawer, Typography, IconButton, List, ListItem, ListItemText, Divider } from '@material-ui/core/';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles } from '@material-ui/core/styles';

import {footernav} from '../../src/store/footernav';



const styles = theme => ({
  root: {
    backgroundColor: 'white',
    boxShadow: '0 3px 5px rgba(0,0,0,0.05)',
    borderTop: '2px solid ' + theme.palette.primary.main
  },
  toolbarTitle: {
    flex: 1,
  },
  mainNav: {
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  sideList: {
    minWidth: '250px',
  },
  menuButton: {
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  divider: {
    marginBottom: theme.spacing.unit*2
  },
  dividerInMenu: {
    marginLeft: theme.spacing.unit*2
  },
  link: {
    margin: theme.spacing.unit*2,
    fontSize: theme.typography.body2.fontSize,
    color: theme.palette.text.primary,
    '&:hover, &:active, &:focus': {
      color: theme.palette.primary.main,
      textDecoration: 'none'
    }
  }
});


const navItems = [{
  title :'Home',
  link: '/'
},{
  title: 'Kontakt',
  link: '/#footer-contact'
},{
  title: 'Leistungen',
  link: '/#content-service'
}];

class Navigation extends React.Component {

  state = {
    drawer: false
  } ;

  toggleDrawer = (open) => () => {
    this.setState({
      drawer: open,
    });
  };

  render() {
    const {classes} = this.props;

    //build desktop nav-list
    const navList = (
      <nav className={classes.mainNav}>
        {navItems.map(item =>
          <Link href={item.link}>
            <a className={classes.link}>{item.title}</a>
          </Link>
        )}
      </nav>
    );

    //build mobile nav-list
    const sideList = (
      <List component="nav" className={classes.sideList}>
        <ListItem>
          <ListItemText primary="Praxis für Logopädie" />
        </ListItem>
        <Divider className={classes.divider}/>
        {navItems.map(item =>
          <Link href={item.link}>
            <ListItem button component="a">
              <ListItemText primary={item.title} />
            </ListItem>
          </Link>
        )}
        <Divider className={classes.dividerInMenu}/>
        {footernav.map(item =>
          <Link href={item.link}>
            <ListItem button component="a">
              <ListItemText primary={item.title} />
            </ListItem>
          </Link>
        )}
      </List>
    )

    return (
      <AppBar position="static" color="default" className={classes.root}>
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
            Praxis für Logopädie
          </Typography>

          {navList}

          <IconButton
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
            onClick={this.toggleDrawer(true)}
            onKeyDown={this.toggleDrawer(true)}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>

          <SwipeableDrawer
            anchor="right"
            open={this.state.drawer}
            onClose={this.toggleDrawer(false)}
            onOpen={this.toggleDrawer(true)}
          >
            <div
              tabIndex={0}
              role="button"
              onClick={this.toggleDrawer(false)}
              onKeyDown={this.toggleDrawer(false)}
            >
              {sideList}
            </div>
          </SwipeableDrawer>

      </AppBar>
    );
  }
}

  export default withStyles(styles)(Navigation);
