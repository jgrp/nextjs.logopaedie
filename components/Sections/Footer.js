
import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link'
import CssBaseline from '@material-ui/core/CssBaseline';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import {Typography} from "@material-ui/core";


const styles = theme => ({
  footer: {
    background: '#3a3d42',
    paddingTop: 1,
  },
  footerContent: {
    paddingBottom: theme.spacing.unit * 6,
    '&::before': {
      backgroundImage: 'url(/static/images/footer-bg.png)',
      backgroundPosition: 'center bottom',
      backgroundRepeat: 'repeat-x',
    },
  },
  footerNav: {
    borderTop: '1px solid #fff',
    textAlign: 'right',
    padding: '1px',
    '&::before': {
      backgroundColor: 'black'
    },
    '& a' : {
      color: 'white',
      paddingTop: theme.spacing.unit,
      paddingBottom: theme.spacing.unit,
      paddingLeft: theme.spacing.unit * 3,
      fontSize: '0.9rem',
      display: 'inline-block',
      '&:hover, &:active, &:focus': {
        color: theme.palette.primary.main,
        textDecoration: 'none'
      }
    }
  },
  transparentBg: {
    position: 'relative',
    zIndex: 2,
    '&::before': {
      content: `''`,
      display: 'block',
      position: 'absolute',
      bottom: 0,
      left: 0 ,
      top: 0,
      right: 0,
      opacity: '0.13',
      pointerEvents: 'none',
      zIndex: 1
    },
  },
  fontColor: {
    color: 'white'
  }
});

class Footer extends React.Component {


  render() {
    const {classes} = this.props;

    return (
      <footer className={classes.footer}>
        <section className={classNames(classes.footerContent, classes.transparentBg)}>
          <div className='container'>
            <Typography variant="h2" className={classes.fontColor} id="footer-contact">Kontakt</Typography>
            <Typography variant="h3" className={classes.fontColor}>Praxis für Logopädie Friedberg</Typography>
            <Typography variant="subtitle1" paragraph={1} className={classes.fontColor}>
              Inhaber Simon Nüchter
            </Typography>

            <Typography paragraph={1} className={classes.fontColor}>
              Lorem ipsum dolor sit amet <br />
              consetetur sadipscing 5 <br />
              123456 Loremipsumdolor <br />
            </Typography>
          </div>
        </section>
        <section className={classNames(classes.footerNav, classes.transparentBg)}>
          <nav className='container'>
            <Link href="/imprint">
              <a>Impressum</a>
            </Link>
            <Link href="/privacy">
              <a>Datenschutz</a>
            </Link>
          </nav>
        </section>
      </footer>

    );
  }
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);

