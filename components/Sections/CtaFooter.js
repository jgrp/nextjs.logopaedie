/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from "classnames";


import PropTypes from "prop-types";
import {Button, Grid, Typography} from "@material-ui/core";

import { Mail } from '@material-ui/icons';



const styles = theme => ({

  ctaFooter: {
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    position: 'relative',
    '&::before': {
      content: `''`,
      display: 'block',
      position: 'absolute',
      left: 0,
      right: 0,
      top: '-200px',
      bottom: 0,
      // height: '150px',
      background: theme.palette.primary.main,
      zIndex: '-2'
    },
    paddingTop: theme.spacing.unit * 6,
    paddingBottom: theme.spacing.unit * 6
  },
  ctaButton: { // TODO: redundant
    boxShadow: 'none',
    backgroundColor: theme.palette.primary.light,
    borderRadius: 0,
  },
  ctaButtonText: {
    marginLeft: theme.spacing.unit,
    position: 'relative',
    top: 1
  },
  headline: {
    marginTop: 0,
  },
  color: {
    color: 'white'
  }

});

class CtaFooter extends React.Component {

  render() {
    const {classes} = this.props;

    return (

      <section className={classes.ctaFooter}>
        <Typography variant="h2" className={classNames(classes.headline, classes.color, 'container')}>Haben Sie Fragen?</Typography>
        <Grid container className='container'>
          <Grid item xs={12} sm={8}>
            <Typography variant="subtitle1" paragraph={1} className={classes.color}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
            </Typography>
            <Typography paragraph={1} className={classes.color}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
            </Typography>
          </Grid>

          <Grid item className="center-element" xs={12} sm={4}>
            <Button variant="contained" size="large" color="primary" className={classes.ctaButton} onClick={this.ctaHandle}>
              <Mail /> <span className={classes.ctaButtonText}>Kontakt aufnehmen</span>
            </Button>
          </Grid>
        </Grid>
      </section>
    );
  }
}

CtaFooter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CtaFooter);
