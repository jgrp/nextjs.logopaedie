/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from "classnames";
import Link from 'next/link'

import {List, ListItem, ListItemIcon, ListItemTextm, Grid} from "@material-ui/core";

import { Smartphone, MailOutline, LocationOn } from '@material-ui/icons';
import PropTypes from "prop-types";



const styles = theme => ({
  icon: {
  },
  text: {
    fontSize: '12px',
    marginLeft:  theme.spacing.unit,
    position: 'relative',
    top: '-8px'
  },
  item: {
    color: '#606a73',
    textAlign: 'center',
    marginRight:  theme.spacing.unit * 4,
    marginLeft:  theme.spacing.unit * 4,
    marginBottom: theme.spacing.unit * 3,
    display: 'block',
    '&:hover, &:active, &:focus': {
      color: theme.palette.primary.main,
      textDecoration: 'none'
    }
  }

});

class AddressItem extends React.Component {

  render() {
    const {classes} = this.props;

    return (

      <a href={this.props.link} target="_blank" className={classes.item}>
        {this.props.icon}
        <span className={classes.text}>{this.props.content}</span>
      </a>
    );
  }
}

AddressItem.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AddressItem);
