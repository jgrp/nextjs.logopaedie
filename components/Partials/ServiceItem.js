/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from "classnames";


const styles = theme => ({
  serviceitem: {
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    fontFamily: theme.typography.headline.fontFamily,
    padding: theme.spacing.unit*2,
    marginBottom: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center',
    // justifyContent: 'space-between',
    textAlign:  'center',
    fontSize: '0.9rem',
    boxShadow: theme.shadows[4],
    borderRadius: theme.shape.borderRadius,
    [theme.breakpoints.up('sm')]: {
      justifyContent: 'center',
      flexDirection: 'column',
      width: theme.spacing.unit * 15,
      height: theme.spacing.unit * 15,
      borderRadius: '50%',
      marginRight: theme.spacing.unit * 3,
      fontSize: '0.8rem',
    },
    [theme.breakpoints.up('md')]: {
      width: theme.spacing.unit * 17,
      height: theme.spacing.unit * 17,
      marginRight: theme.spacing.unit * 4,
      fontSize: '0.9rem',
    },
    '&:last-of-type': {
      marginRight: 0
    }
  },
  title: {
    fontSize: '1.4rem',
    marginRight: theme.spacing.unit,
    [theme.breakpoints.up('sm')]: {
      display: 'block',
      marginRight: 0,
    },
  },
  subtitle: {
    position: 'relative',
    top: 2
  },
  blue: {
    backgroundColor: theme.palette.secondary.main
  },
  violett: {
    backgroundColor: theme.palette.action.active
  }
});

class ServiceItem extends React.Component {

  render() {
    const {classes} = this.props;

    return (
      <div className={classNames(classes.serviceitem,
        { [classes.blue]: this.props.color === 'blue'},
        { [classes.violett]: this.props.color === 'violett' }
        )}>
        <span className={classes.title}>{this.props.content}</span>
        <span className={classes.subtitle}> therapie</span></div>
    );
  }
}

export default withStyles(styles)(ServiceItem);
