export const serviceChildren = [
  'Sprachentwicklungsstörung und -verzögerung',
  'Artikulationsstörung (Dyslalien)',
  'Dysgrammatismus',
  'Wortabrufstörung',
  'Auditive Wahrnehmungsstörung',
  'Störung des Schriftspracherwerbs',
  'Juvenile Dysphonie',
  'Myofunktionelle Störung'
];

export const serviceAdults = [
  'Aphasie',
  'Sprechapraxie',
  'Dysarthrophonie',
  'Facialisparese',
  'Dysphonie (Stimmstörung)',
  'Störungen des Redeflusses (Stottern und Poltern)'
];