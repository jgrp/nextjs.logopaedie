/* eslint-disable no-underscore-dangle */

import { SheetsRegistry } from 'jss';
import { createMuiTheme, createGenerateClassName } from '@material-ui/core/styles';

// A theme with custom primary and secondary color.
// It's optional.
const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#38d3c1',
      main: '#499f94',
      // dark: purple[700],
    },
    secondary: {
      // light: green[300],
      main: '#398da9',
      // dark: green[700],
    },
    action: {
      active: '#5f518a',
    },
    text: {
      primary: '#101923'
    },
    background: {
      default: '#fff'
    }
  },
  typography: {
    useNextVariants: true,
    fontFamily: "'Libre Franklin', 'sans-serif'",
    headline: {
      fontFamily: '"Quattrocento", serif'
    },
    h1: {
      fontFamily: '"Quattrocento", serif',
      fontSize: '36px'
    },
    h2: {
      fontFamily: '"Quattrocento", serif',
      fontSize: '34px',
      color: '#398da9', // secondary-main
      marginTop: '88px'
    },
    h3: {
      fontFamily: '"Quattrocento", serif',
      fontSize: '23px',
      marginTop: '68px'
    },
    h4: {
      fontFamily: '"Quattrocento", serif',
      marginTop: '42px'
    },
    subtitle1: {
      marginTop: '8px'
    }
  },
});

function createPageContext() {
  return {
    theme,
    // This is needed in order to deduplicate the injection of CSS in the page.
    sheetsManager: new Map(),
    // This is needed in order to inject the critical CSS.
    sheetsRegistry: new SheetsRegistry(),
    // The standard class name generator.
    generateClassName: createGenerateClassName(),
  };
}

export default function getPageContext() {
  // Make sure to create a new context for every server-side request so that data
  // isn't shared between connections (which would be bad).
  if (!process.browser) {
    return createPageContext();
  }

  // Reuse context on the client-side.
  if (!global.__INIT_MATERIAL_UI__) {
    global.__INIT_MATERIAL_UI__ = createPageContext();
  }

  return global.__INIT_MATERIAL_UI__;
}
