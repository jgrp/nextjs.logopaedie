/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Link from 'next/link';
import Layout from '../components/Layout.js'
import classNames from "classnames";
import {imprintContent} from '../src/store/imprint';

const styles = theme => ({
  '@global' : {
    'body': {
      fontFamily: '"Libre Franklin", "sans-serif"',
      color: '#101923'
    },
    '.container': {
      maxWidth: '1200px',
      marginLeft: 'auto',
      marginRight: 'auto',
      paddingLeft: '16px',
      paddingRight: '16px',
    },
    'a': {
      color: '#499f94',
      textDecoration: 'none',

      '&:hover, &:visited, &:active, &:focus': {
        textDecoration: 'underline',
      }
    },
    '.center-element': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    'section.content': {
      paddingTop: theme.spacing.unit * 10,
      paddingBottom: theme.spacing.unit * 10,
    },
  },
});

function About(props) {
  const { classes } = props;

  return (
    <Layout>
      <section className={classNames('content', 'container')}>

          <Typography variant="h2">Impressum</Typography>
          <Typography variant="h3">Vertantwortlich</Typography>
          <Typography variant="subtitle1" paragraph={1}>
            Praxis für Logopädie Friedberg <br/>
            Simon Nüchter
          </Typography>
          <Typography paragraph={1}>
            Grüner Weg 9 <br/>
            61169 Friedberg
          </Typography>
          <Typography paragraph={1}>
            Telefon: 06031 161718 <br/>
            E-Mail: info(a)friedberg-logopädie.de
          </Typography>

          {imprintContent.map(group =>
            <div>
              <Typography variant="h3">{group.title}</Typography>
              <Typography>{group.content}</Typography>
            </div>
          )}

          <Typography paragraph={1}>
            Quelle: Disclaimer eRecht24.de Informationsportal
          </Typography>

      </section>

    </Layout>
  );
}

About.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(About);
