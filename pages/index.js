/* eslint-disable jsx-a11y/anchor-is-valid */

import PropTypes from 'prop-types';
import {
  Button,
  Typography,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Grid
} from '@material-ui/core';
import { Smartphone, MailOutline, LocationOn, Star, DateRange } from '@material-ui/icons';


import CheckIcon from '@material-ui/icons/Check';

import { withStyles } from '@material-ui/core/styles';
import Link from 'next/link';
import React from "react";
import classNames from "classnames";

import Layout from '../components/Layout.js'
import ServiceItem from "../components/Partials/ServiceItem";
import AddressItem from "../components/Partials/AddressItem";
import CtaFooter from "../components/Sections/CtaFooter";
import Header from '../components/Sections/Header';

import {serviceChildren, serviceAdults} from '../src/store/index';


const styles = theme => ({

  root: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 20,
  },
  mainContent: {
    // marginTop: theme.spacing.unit * 15,
    paddingBottom: theme.spacing.unit * 10,
    position: 'relative',
    // overflow: 'hidden',
    '&::before': {
      content: `''`,
      display: 'block',
      position: 'absolute',
      bottom: 0,
      left: 0 ,
      top: '-168px',
      width: '100%' ,
      background: theme.palette.background.default,
      zIndex: '-1',

      [theme.breakpoints.up('xs')]: {
        borderRadius: '30% 45% 45% 30%/7px 75px 50px 5px',
      },
      [theme.breakpoints.up('sm')]: {
        borderRadius: '30% 45% 45% 30%/10px 100px 70px 7px',
      },
      [theme.breakpoints.up('md')]: {
        borderRadius: '30% 45% 45% 30%/15px 150px 120px 10px',
      },
      [theme.breakpoints.up('lg')]:{
        borderRadius: '30% 45% 45% 30%/15px 200px 150px 10px',
      },

    }
  },
  info: {
    paddingTop: theme.spacing.unit * 3,
    position: 'relative',
    '&::before': {
      content: `''`,
      display: 'block',
      position: 'absolute',
      left: 0,
      right: 0,
      top: '-200px',
      bottom: '-200px',
      // height: '150px',
      background: '#eef3f7',
      zIndex: '-2'
    }
  },
  flexCenter: {
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    },
  },
  ctaButton: {// TODO: redundant
    boxShadow: 'none',
    backgroundColor: theme.palette.primary.light,
    borderRadius: 0
  },
  ctaButtonText: {// TODO: redundant
    marginLeft: theme.spacing.unit,
    position: 'relative',
    top: 2
  },
  starsContainer: {
    display: 'flex',
    color: '#e9b130'
  },

  '@global' : {
    'body': {
      fontFamily: '"Libre Franklin", "sans-serif"',
      color: '#101923'
    },
    '.container': {
      maxWidth: '1200px',
      marginLeft: 'auto',
      marginRight: 'auto',
      paddingLeft: '16px',
      paddingRight: '16px',
    },
    'a': {
      color: '#499f94',
      textDecoration: 'none',

      '&:hover, &:visited, &:active, &:focus': {
        textDecoration: 'underline',
      }
    },
    '.center-element': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    'section.content': {
      paddingTop: theme.spacing.unit * 10,
      paddingBottom: theme.spacing.unit * 10,
    },
   },
});


class Index extends React.Component {
  state = {
    open: false,
  };

  handleClose = () => {
    this.setState({
      open: false,
    });
  };

  handleClick = () => {
    this.setState({
      open: true,
    });
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;


    return (
      <Layout>

        <Header />

        <section className={classes.info}>
          <div className='container'>
            <div className={classes.flexCenter}>
              <AddressItem icon={<Smartphone />} content="06031 / 161718" link="tel:+49603161718" />
              <AddressItem icon={<MailOutline />} content="info(a)friedberg-logopaedie.de" link="mailto:info(a)friedberg-logopaedie.de" />
              <AddressItem icon={<LocationOn />} content="Grüner Weg 9, 61169 Friedberg" link="https://www.google.de/maps/place/Praxis+f%C3%BCr+Logop%C3%A4die+Friedberg+-+N%C3%BCchter+u.+Metzger/@50.325463,8.7435022,17z/data=!4m8!1m2!2m1!1slogop%C3%A4die+in+der+N%C3%A4he+von+Friedberg!3m4!1s0x47bd02236ee88955:0xa7966702e4bc4a7b!8m2!3d50.325463!4d8.7456909" />
            </div>
            <div className={classes.flexCenter}>
              <ServiceItem content="Stimm" color="violett" />
              <ServiceItem content="Sprach" color="blue" />
              <ServiceItem content="Sprech"/>
              <ServiceItem content="Schluck" color="blue" />
              <ServiceItem content="Hör" color="violett" />
            </div>
          </div>
        </section>

        <section className={classes.mainContent}>

          <div className='container'>
            <Typography variant="h2">Lorem ipsum</Typography>
            <Grid container>
              <Grid item xs={12} sm={8}>

                <Typography variant="subtitle1" paragraph={1}>
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                </Typography>
                <Typography paragraph={1}>
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                  <Link href="/" color="primary"><a>Link</a></Link>
                </Typography>

              </Grid>
              <Grid item className="center-element" xs={12} sm={4}>

                <Button variant="contained" size="large" color="primary" className={classes.ctaButton} onClick={this.ctaHandle}>
                  <DateRange /><span className={classes.ctaButtonText}>Jetzt Termin vereinbaren</span>
                </Button>

              </Grid>
            </Grid>
          </div>

          <div className={classNames(classes.bodytext, 'container')} id="content-service">
            <Typography variant="h2">Leistungen</Typography>
            <Typography variant="subtitle1" paragraph={1}>
              In unserer Praxis bietet unser Team die Therapie der verschiedenen logopädischen Störungsbilder an.
            </Typography>


            <Grid container>
              <Grid item sm={12} md={6}>

                <Typography variant="h3">Kinder</Typography>
                 <List>
                   {serviceChildren.map(group =>
                    <ListItem> <ListItemIcon><CheckIcon color="action" /></ListItemIcon> <ListItemText primary={group} /></ListItem>
                   )}
                  </List>
              </Grid>
              <Grid item sm={12} md={6}>
                <Typography variant="h3">Erwachsene</Typography>
                <List>
                  {serviceAdults.map(group =>
                    <ListItem> <ListItemIcon><CheckIcon color="action" /></ListItemIcon> <ListItemText primary={group} /></ListItem>
                  )}
                </List>
              </Grid>
            </Grid>
          </div>
          <div className={classNames(classes.bodytext, 'container')}>
            <Typography variant="h3">Cochlea Implantat</Typography>
            <Typography variant="subtitle1" paragraph={1}>
              Exklusiver Partner des Cochlear Implant Centrum Rhein-Main
            </Typography>

            <Typography paragraph={1}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
            </Typography>

            <Typography paragraph={1}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
            </Typography>

          </div>

          <div className={classNames(classes.bodytext, 'container')}>
            <Typography variant="h3">Erfahrungen</Typography>

            <div className={classes.flexCenter} style={{wrap: 'nowrap'}}>
              <div className={classes.starsContainer}><Star /><Star /><Star /><Star /><Star /></div>
              <blockquote>
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
              </blockquote>
            </div>
          </div>

        </section>

        <CtaFooter/>


      </Layout>
    );
  }
}

Index.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Index);
