## webpack 
https://medium.com/javascript-training/beginner-s-guide-to-webpack-b1f1a3638460
https://github.com/material-components/material-components-web/blob/master/docs/getting-started.md
https://github.com/petehunt/webpack-howto


## setup on windwos

- install webpack global `npm install webpack -g`
- install webpack-cli global `npm i -g webpack-cli`
- install webpack dev server `npm install webpack-dev-server -g`  With Webpack dev server running, you will notice that if you go back to your app and make a change, the browser will automatically refresh (hot-loading).

## setup vagrant

https://github.com/scotch-io/scotch-box

`chmod 600 ~/.ssh/id_rsa`



## Next.js

npm run dev


npm install -g serve
serve -p 8080


### Export static
npm run build
npm run export


## Material UI Tutorial

https://github.com/alex996/react-exercises
https://codesandbox.io/s/qq4oz0ym69




